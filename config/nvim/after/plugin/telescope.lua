local ignore_filetypes_list = {
    "venv", "__pycache__", "%.xlsx", "%.jpg", "%.png", "%.webp",
    "%.pdf", "%.odt", "%.ico",
}

local builtin = require('telescope.builtin')

require('telescope').setup{
    defaults = {
        file_ignore_patterns = ignore_filetypes_list,
    },
    pickers = {
        find_files = {
            find_command = { 'rg', '--files', '--iglob', '!.git', '--hidden' },
        },
        grep_string = {
            additional_args = {'--hidden'}
        },
        live_grep = {
            additional_args = {'--hidden'}
        }
    }
}
vim.keymap.set('n', '<leader>pf', builtin.find_files, {})
vim.keymap.set('n', '<C-p>', builtin.git_files, {})
vim.keymap.set('n', '<leader>fb', builtin.buffers, {})

vim.keymap.set('n', '<leader>ps', function()
  builtin.grep_string({ search = vim.fn.input("grep > ")})
end)

