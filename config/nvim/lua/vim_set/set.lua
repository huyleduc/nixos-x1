vim.opt.guicursor = ""

vim.opt.nu = true
vim.opt.relativenumber = true

vim.opt.tabstop = 2
vim.opt.softtabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = true

vim.opt.smartindent = true

vim.opt.wrap = false

vim.opt.swapfile = false
vim.opt.backup = false
--vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

vim.opt.hlsearch = false
vim.opt.incsearch = true

vim.opt.termguicolors = true

vim.opt.scrolloff = 8
vim.opt.signcolumn = "yes"
vim.opt.isfname:append("@-@")

vim.opt.updatetime = 50

vim.opt.colorcolumn = "81"

vim.cmd('set t_ZH=[3m')
vim.cmd('set t_ZR=[23m')

-- Neovim config for the links to show properly
vim.opt.conceallevel = 2
vim.opt.concealcursor = "nc"
--vim.opt.clipboard:append("unnamed")



vim.o.clipboard = "unnamedplus"
vim.cmd([[autocmd BufWritePre * %s/\s\+$//e]])
vim.o.termguicolors = true
--vim.o.t_Color = 256

-- vim.cmd.colorscheme("rose-pine")

