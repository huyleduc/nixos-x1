#!/usr/bin/env bash

project_directories=(
  ~/Project
  ~/Project/gitlab
  ~/Project/homelab_gitlab
)

# Convert array to space-separated string
directories_string=$(IFS=' ' && echo "${project_directories[*]}")
if [[ $# -eq 1 ]]; then
    selected=$1
else
    selected=$(find $directories_string -mindepth 1 -maxdepth 1 -type d | fzf)
fi

if [[ -z $selected ]]; then
    exit 0
fi

tmux split-window -c "$selected"

