#!/bin/bash

# Define the root folder as the current working directory
root_folder="$PWD"

# Use find to get a list of all PDF files and pipe it to fzf for interactive selection
selected_file=$(find "$root_folder" -type f -name "*.pdf" -printf "%P\n" | fzf)

# Check if a file was selected
if [ -n "$selected_file" ]; then
    echo "Selected PDF file: $root_folder/$selected_file"
    zathura "$root_folder/$selected_file" &

else
    echo "No PDF file selected."
fi


