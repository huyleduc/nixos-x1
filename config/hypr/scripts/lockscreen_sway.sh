#!/usr/bin/env bash
# Check if swaylock is already running
MONITOR_COUNT=$(hyprctl monitors | grep 'Monitor' | wc -l)

# Check if swaylock is already running
if pgrep -x "swaylock" > /dev/null; then
    echo "swaylock is already running."
elif [ "$MONITOR_COUNT" -le 1 ]; then
    swaylock \
        --image $(dirname "$0")/img/blockwavemoon.png \
        --indicator-radius 100 \
        --indicator-thickness 7 \
        --key-hl-color eb6f92 \
        --line-color 00000000 \
        --ring-color c4a7e7 \
        --text-color e0def4 \
        --inside-color 00000088 \
        --separator-color 00000000 \
        --inside-ver-color 31748f \
        --ring-ver-color 31748f
fi




#--screenshots \
# --effect-blur 7x5 \
# --grace 2 \
#--fade-in 0.2 \
