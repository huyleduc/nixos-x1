#!/usr/bin/env bash

# Function to print a color block with its number, right-aligned
print_color() {
    local color=$1
    # Right-align using printf, allocating 4 spaces for the color number
    printf "\e[48;5;${color}m\e[38;5;15m%4s\e[0m" "$color"
}

echo "ANSI 256 Colors:"
echo "----------------"

# Iterate through the 256 colors
counter=0
for color in {0..255}; do
    print_color $color
    echo -n " "
    ((counter++))
    if (( counter % 16 == 0 )); then
        echo ""
    fi
done
echo ""

