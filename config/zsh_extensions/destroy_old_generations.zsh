#!/usr/bin/env bash

# Prompt the user before proceedin
echo "Listing all generations before deletion..."
sudo nix-env -p /nix/var/nix/profiles/system --list-generations
read -p "Do you want to delete all generations? (y/N) " answer

case $answer in
    [yY]* )
        echo "Proceeding with deletion..."
        sudo nix-collect-garbage -d
        nix-collect-garbage -d
        ;;
    * )
        echo "Deletion cancelled."
        ;;
esac
