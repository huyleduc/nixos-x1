if [ -n "${commands[fzf-share]}" ]; then
  source "$(fzf-share)/key-bindings.zsh"
  source "$(fzf-share)/completion.zsh"
fi

# Append a command directly
#zvm_after_init_commands+=("[ -f $(fzf)/.fzf.zsh ] && source $(fzf)/.fzf.zsh")
