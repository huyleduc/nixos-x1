#!/usr/bin/env bash
#
# Set image path variable
IMAGE_PATH="/.config/hypr/wallpaper/zenPineMoon.png"

# Set the output file path
OUTPUT_FILE="/home/huy/.config/hypr/hyprpaper.conf"

# Fetch the list of monitors
MONITORS=$(hyprctl monitors | grep 'Monitor' | awk '{ print $2 }')

# Start generating the file content
echo "preload = $IMAGE_PATH" > "$OUTPUT_FILE"

# Loop through each monitor and append it to the file
for MONITOR in $MONITORS; do
    echo "wallpaper = $MONITOR,$IMAGE_PATH" >> "$OUTPUT_FILE"
done

# Restart hyprpaper to apply changes
pkill hyprpaper
hyprpaper &



