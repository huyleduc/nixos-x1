#!/usr/bin/env bash
#
# Set image path variable

# Set the path to the target file as a variable
FILE_PATH="/home/huy/.config/nvim/after/plugin/colors.lua"  # Modify this to your desired path

# Set the target string and its replacement as variables
SEARCH_STRING="    color = color or 'rose-pine-"
REPLACE_STRING="    color = color or 'rose-pine-moon'"
# Check if the file exists
if [ ! -f "$FILE_PATH" ]; then
    echo "Error: File '$FILE_PATH' does not exist."
    exit 2
fi

# Find the line number of the line containing the search string
LINE_NUM=$(grep -n "$SEARCH_STRING" "$FILE_PATH" | cut -f1 -d:)

# If the line number is not empty, proceed to replace the line
if [ ! -z "$LINE_NUM" ]; then
    sed -i "${LINE_NUM}s|.*|${REPLACE_STRING}|" "$FILE_PATH"
    echo "Replacement done in '$FILE_PATH'."
else
    echo "No line containing the search string found."
fi



