{ pkgs, unstable, ... }:

let
  nvimConfigDir = builtins.path {
    path = ./../../config/nvim; # Update this path as needed
    name = "nvim-config";
  };

  nvimTreesitterPlugin = import ./nvim_treesitter.nix { inherit pkgs; };
in
{
  home-manager.users.huy = {
    home.sessionVariables = {
      EDITOR = "nvim";
    };
    home.packages = with pkgs.unstable; [
      gopls
      lua-language-server
      marksman
      nil
      pyright
      ripgrep
      texliveFull
    ];

    home.file.".config/nvim".source = nvimConfigDir;
    programs.neovim = {
      enable = true;
      defaultEditor = true;
      viAlias = true;
      vimAlias = true;
      extraLuaPackages = luaPkgs: with luaPkgs; [
      lua-utils-nvim
      pathlib-nvim
      jsregexp
      ];
      plugins = with pkgs.unstable.vimPlugins; [
        nvimTreesitterPlugin # This imports your nvim-treesitter configuration
        catppuccin-nvim
        cmp-nvim-lsp
        cmp_luasnip
        lsp-zero-nvim
        lualine-nvim
        neorg
        nvim-cmp
        nvim-lspconfig
        nvim-web-devicons
        plenary-nvim
        rose-pine
        telescope-nvim
        undotree
        vim-fugitive
        vimtex
      ];
    };
  };
}

