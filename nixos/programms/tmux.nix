{ pkgs, ... }:
let
  # Assuming this Nix file is located at /path/to/your/repo/nixfiles/home.nix
  # and you want to reference /path/to/your/repo/config/tmux relatively
  configDir = builtins.path {
    path = ./../../config/tmux;
    name = "tmux-config";
  };
in
{
  home-manager.users.huy = {
    programs.tmux = {
      enable = true;
    };

    home.file.".config/tmux".source = configDir;

    home.packages = with pkgs; [
      xclip
      # any other packages your tmux config needs
    ];
  };
}
