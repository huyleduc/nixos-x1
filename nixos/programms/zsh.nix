{ pkgs, ... }: let
  zshExtensionsDir = builtins.path {
    path = ./../../config/zsh_extensions;
    name = "zsh-config";
  };
  p10kDir = builtins.path {
    path = ./../../config/p10k;
    name = "p10k-config";
  };
in
{

    home-manager.users.huy ={

        programs.zsh = {
            enable = true;
            #enableAutosuggestions = true;
            autosuggestion.enable = true;
            syntaxHighlighting.enable = true;
            enableCompletion = true;
            dotDir = ".config/zsh";

            history = {
                ignoreAllDups = true;
                save = 10000;
                size = 10000;
                share = true;
            };
            historySubstringSearch = {
                enable = true;
            };
            loginExtra = ''
                ~/.config/hypr/scripts/single_hypr.sh
                '';
            initExtraBeforeCompInit = ''
                source ${zshExtensionsDir}/grml.zsh
                '';
            initExtra = ''
                source ${zshExtensionsDir}/vim-mode.zsh
                source ${zshExtensionsDir}/fzf.zsh
                '';

            shellAliases = {
                flakeNixosBuild = "sudo nixos-rebuild switch --flake /home/huy/Project/gitlab/nixos-x1/nixos";
                unimatrix = "~/.local/share/unimatrix";
                destroy_old_generations = "${zshExtensionsDir}/destroy_old_generations.zsh";
                termColorsAnsii = "${zshExtensionsDir}/termColorsAnsii.sh";
            };

            plugins = [
            {
                name = "powerlevel10k";
                src = pkgs.zsh-powerlevel10k;
                file = "share/zsh-powerlevel10k/powerlevel10k.zsh-theme";
            }
            {
                name = "powerlevel10k-config";
                src = "${p10kDir}";
                file = "config.zsh";
            }
            ];
        };
    };
}


