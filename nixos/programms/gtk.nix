{ pkgs, ... }: {
    home-manager.users.huy = { # Replace 'yourUserName' with your actual user name
        gtk = {
            enable = true;
            font = {
                package = pkgs.nerdfonts.override { fonts = [ "Mononoki" ]; };
                name = "Mononoki Nerd Font Regular";
                size = 12;
            };
            iconTheme = {
                package = pkgs.catppuccin-papirus-folders.override { flavor = "mocha"; accent = "peach"; };
                name = "Papirus-Dark";
            };
            theme = {
                name = "Catppuccin-Macchiato-Compact-Lavender-Dark";
                package = pkgs.catppuccin-gtk.override {
                    accents = [ "lavender" ];
                    size = "compact";
                    #tweaks = [ "rimless" "black" ];
                    tweaks = [ "normal" ];
                    variant = "macchiato";
                };
            };
        };
    };
}
