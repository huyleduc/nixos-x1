{ pkgs, ... }:
let
  # Define the path to your Alacritty configuration directory relative to this Nix file
  alacrittyConfigDir = builtins.path {
    path = ./../../config/alacritty; # Update this path as needed
    name = "alacritty-config";
  };
in
{
  home-manager.users.huy = {
    # Optionally include Alacritty in your environment if it's not already installed
#    home.packages = with pkgs; [
#      alacritty
#      # any other packages needed
#    ];

    # Create a symlink from ~/.config/alacritty to your Alacritty configuration directory
    home.file.".config/alacritty".source = alacrittyConfigDir;
  };
}

