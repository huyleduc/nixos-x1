{ pkgs, ... }:
let
  # Define the path to your wofi configuration directory relative to this Nix file
  wofiConfigDir = builtins.path {
    path = ./../../config/wofi; # Update this path as needed
    name = "wofi-config";
  };
in
{
  home-manager.users.huy = {
    # Create a symlink from ~/.config/wofi to your wofi configuration directory
    home.file.".config/wofi".source = wofiConfigDir;

    # Include wofi in your environment if it's not already installed or if you need a specific version
    home.packages = with pkgs; [
      wofi
      # any other packages needed
    ];
  };
}

