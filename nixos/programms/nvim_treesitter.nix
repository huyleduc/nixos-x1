{ pkgs }:

let

# Define a list of programming languages you want to include
languages = [
  "bash"
  "c"
  "go"
  "gitcommit"
  "java"
  "javascript"
  "lua"
  "markdown"
  "markdown_inline"
  "nix"
  "norg"
  "python"
  "rasi"
  "vim"
  "vimdoc"
];

  # Function to map over the languages and fetch the corresponding plugin
  getPlugins = p: map (lang: p."${lang}") languages;
in
pkgs.unstable.vimPlugins.nvim-treesitter.withPlugins (p: getPlugins p)

