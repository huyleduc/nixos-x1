{ pkgs, ... }:
let
  # Define the path to your wofi configuration directory relative to this Nix file
  rofiConfigDir = builtins.path {
    path = ./../../config/rofi_wayland; # Update this path as needed
    name = "rofi-config";
  };
in
{
  home-manager.users.huy = {
    # Create a symlink from ~/.config/wofi to your wofi configuration directory
    home.file.".config/rofi".source = rofiConfigDir;

    # Include wofi in your environment if it's not already installed or if you need a specific version
    home.packages = with pkgs; [
      rofi-wayland
      # any other packages needed
    ];
  };
}
