{pkgs, unstable, ... }:
let
  waybarConfigDir = builtins.path {
    path = ./../../config/waybar; # Update this path as needed
    name = "waybar-config";
};
in
{
    home-manager.users.huy ={
        programs.waybar = {
            enable = true;
# package = pkgs.unstable.waybar.override  {
#     hyprlandSupport = true;
# };
        };

        home.file.".config/waybar".source = waybarConfigDir;
    };
}
