{pkgs, hyprland, unstable, ... }:
let
  hyprlandConfigDir = builtins.path {
    path = ./../../config/hypr; # Update this path as needed
    name = "hyprland-config";
  };
in
{
    home-manager.users.huy = {
        home.file.".config/hypr".source = hyprlandConfigDir;
#        wayland.windowManager.hyprland = {
#            enable = true;
#            systemd.enable = true;
#            package = pkgs.hyprland;
#            # Whether to enable XWayland
#            xwayland.enable = true;
#            extraConfig =''
#            # hello
#                '';
#
#        };
#        programs.waybar = {
#            enable = true;
#            package = pkgs.unstable.waybar.override  {
#                hyprlandSupport = true;
#            };
#        };
    };
}
