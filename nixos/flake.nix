{
  description = "Starting Home-manger";


  inputs = {
    nixosPkgs.url = "github:nixos/nixpkgs/nixos-24.11";
    unstable.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixosPkgs";
    };
    hyprland.url = "github:hyprwm/Hyprland";

  };

  outputs = { self, nixosPkgs, unstable, home-manager, hyprland } @ args  : {
      nixosConfigurations.nixos = nixosPkgs.lib.nixosSystem {
        specialArgs = args;
        system = "x86_x64-linux";
        modules = let
        overlay-unstable = final: prev: {
          unstable = unstable.legacyPackages.${prev.system};
            };
        in

        [
            ./configuration.nix
            home-manager.nixosModule
            ./programms/alacritty.nix
            ./programms/gtk.nix
            ./programms/hyprland.nix
            ./programms/nvim.nix
            ./programms/rofi_wayland.nix
            ./programms/tmux.nix
            ./programms/waybar.nix
            ./programms/wofi.nix
            ./programms/zsh.nix
            (_: {nixpkgs.overlays = [overlay-unstable];})
        ];
      };
  };
}
