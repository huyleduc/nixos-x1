# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running `nixos-help`).

{ config, pkgs,  ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  nix.gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 14d";
  };

  networking.hostName = "nixos"; # Define your hostname.
  # Pick only one of the below networking options.
  #networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.

  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  # i18n.defaultLocale = "en_US.UTF-8";
  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  #   useXkbConfig = true; # use xkbOptions in tty.
  # };

  # Enable the X11 windowing system.
  # services.xserver.enable = true;
  xdg.portal.enable = true;
  xdg.portal.extraPortals = [ pkgs.xdg-desktop-portal-gtk ];

  # Configure keymap in X11
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e,caps:escape";

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  # sound.enable = true;
  # hardware.pulseaudio.enable = true;
  hardware.bluetooth.enable = true;
  services.blueman.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;
  users.defaultUserShell = pkgs.zsh;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.huy = {
    isNormalUser = true;
    extraGroups = [
      "wheel"
      "networkmanager"
      "input"
    ]; # Enable ‘sudo’ for the user.
    packages = with pkgs; [
      bitwarden
      bitwarden-cli
      chromium
      cmatrix
      darkman
      dunst
      feh
      firefox
      gotop
      gtypist
      hyprpaper
      inkscape
      neofetch
      networkmanagerapplet
      pandoc
      poppler_utils
      thunderbird
      tree
      tty-clock
      ttyper
      typer
      wl-clipboard
      ventoy
      xclip
      zathura
    ];
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
     alacritty
     brightnessctl
     cmake
     gcc
     gdb
     geoclue2
     git
     glib
     gnumake
     go
     jdk17
     jq
     nodejs
     pango
     pavucontrol
     pipewire
     python3
     rustup
     swaylock
     tmux
     unstable.fzf
     unzip
     wget
     zsh
   ];
   environment.shells = with pkgs; [zsh];

   programs.zsh = {
       enable = true;
       autosuggestions.enable = true;
       shellAliases = {
           ll = "ls -la";
           update = "sudo nixos-rebuild switch";
       };
       histSize = 10000;
   };
   programs.hyprland.enable = true;
   #programs.neovim.defaultEditor = true;
   environment.variables.EDITOR = "nvim";

   #sound.enable = true;
   security.rtkit.enable = true;
   services.pipewire = {
     enable = true;
     alsa.enable = true;
     alsa.support32Bit = true;
     pulse.enable = true;
     jack.enable = true;
   };



   fonts.packages = with pkgs; [
     fira-code
     fira-code-symbols
     envypn-font
     iosevka
     (nerdfonts.override { fonts = [ "FiraCode" "DroidSansMono" ]; })
   ];

  security.pam.services.swaylock = {};
  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  system.stateVersion = "24.11"; # Did you read the comment?
  home-manager.users.huy.home.stateVersion = "24.11";

}
